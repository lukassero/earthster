import { Controller } from "stimulus"
import { template } from "lodash"

// data
const data = {
  name: "Inkjet Printer",
  impacts: {
    climate_change: {
      production: 100,
      distribution: 23,
      use: 12,
      end_of_life: 35,
    },
    energy_use: {
      production: 32,
      distribution: 75,
      use: 28,
      end_of_life: 1
    }
  }
}

const colors = ["red", "orange", "white", "turquoise", "coral", "blue", "pink"];

export default class extends Controller {
  static get targets() {
    return ['name', 'options', 'svgTemplate', 'svg'];
  }

  connect() {
    this.setName();
    this.populateOptions();
    this.drawSwg();
  }

  setName() {
    this.nameTarget.textContent = data.name;
  }

  populateOptions() {
    var selectOptions = "";

    Object.keys(data.impacts).forEach(
      item => {
        selectOptions += "<option value='" + item +"'>" + item + "</option>";
      }
    );

    this.optionsTarget.innerHTML = selectOptions;
  }

  drawSwg() {
    // get selected option
    var selectedOption = document.getElementById("options").value;

    // https://mikerogers.io/2020/06/07/how-to-interpolate-templates-in-stimulus
    // https://lodash.com/docs/4.17.15#template
    // Convert the contents of the template tag, into a lodash template:

    // setup svg template
    this.svgTemplate = template(this.svgTemplateTarget.innerHTML);

    var svg = "";
    var svgWidth = 216;
    var svgBarWidth = 30;
    var i = 0;

    var svgData = data.impacts[selectedOption];

    // draw svg
    var svgBarLength = Object.keys(svgData).length;

    for (var key in svgData) {
      this.svgConfig = {
        itemValue: svgData[key],
        itemName: key.charAt(0).toUpperCase(),
        itemPosition: i * (svgBarWidth + (svgWidth - svgBarLength * svgBarWidth) / (svgBarLength - 1)),
        itemWidth: svgBarWidth,
        itemColor: colors[i]
      };
      svg += this.svgTemplate(this.svgConfig);
      i++;
    }

    this.svgTarget.innerHTML = svg;
  }
}
