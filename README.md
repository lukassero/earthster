# Coding challenge 

## Yo, hello!
- it is the first time I work with Tailwind, but I like it ;]
- text, border and background colors are not quite the same, I used what Tailwind offers - didn't want to write custom CSS, nor I had time to look up the precise shades of gray
- paddings are quite close to the original
- native select - could potentially play and slightly customize it
- stimulus js is cool
- SVG is generated using a template
- had to close <rect></rect> like so, otherwise, it was closing a tag in a wrong way
- text is wrapped in an extra SVG tag so it is easier to center it against the rectangle
- bars are evenly distributed, you can play with them by amending the number of JSON entries, e.g.
```
climate_change: {
  production: 100,
  distribution: 23,
  use: 12,
},
```
- haven't added any animation, as this is a bit tricky (it is already pretty late) - animating a bar from bottom to top would require some clip-path or transform rotate or animating height and y position at the same time etc.
- some JS refactor needed I guess 

## To run a project
```
$ yarn install
$ yarn start
```

## Impact graphs

![Impact Graphs](images/layout_impact_graphs.png)

- Clone this project 
- Write a HTML page that follows the mockup above and uses the following JSON data:

```javascript
{
  name: "Inkjet Printer",
  impacts: {
    climate_change: {
      production: 45,
      distribution: 23,
      use: 12,
      end_of_life: 5
    },
    energy_use: {
      production: 32,
      distribution: 5,
      use: 28,
      end_of_life: 1
    }
  }
}
```

- By default it should generate a SVG graph based on the data from the first impact (eg. climate_change). 
- Try to get the bar graph as close as possible to the mockup.
- The select element should be filled with all available impacts (eg. climate_change and energy_use).
- The graph will be redraw when the user selects a different option.
- You may use any framework but we are giving bonus points for Tailwind CSS, Stimulus JS and HTML5 SVG.
- You may ask any question regarding the challenge. Good communication is paramount.
- Send us the link to your project, we will use it during an online interview. 

Happy coding!

Earthster Team


<hr />

## Stimulus Starter

A preconfigured blank slate for exploring [Stimulus](https://github.com/hotwired/stimulus). Jump to [The Stimulus Handbook](https://stimulus.hotwire.dev/handbook/introduction) for an introduction.

---

We recommend [remixing `stimulus-starter` on Glitch](https://glitch.com/edit/#!/import/git?url=https://github.com/hotwired/stimulus-starter.git) so you can work entirely in your browser without installing anything:

[![Remix on Glitch](https://cdn.glitch.com/2703baf2-b643-4da7-ab91-7ee2a2d00b5b%2Fremix-button.svg)](https://glitch.com/edit/#!/import/git?url=https://github.com/hotwired/stimulus-starter.git)

Or, if you'd prefer to work from the comfort of your own text editor, you'll need to clone and set up `stimulus-starter`:

```
$ git clone https://github.com/hotwired/stimulus-starter.git
$ cd stimulus-starter
$ yarn install
$ yarn start
```

---

© 2020 Basecamp, LLC.
